var Bicicleta = require('../../models/bicicleta');

beforeEach(() => { Bicicleta.allBicis = [] });

describe('Bicicleta.allBicis', () => {
	it('Comienza vacía', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});

describe('Bicicleta.add', () => {
	it('Agregamos una', () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
		Bicicleta.add(a);

		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(a);
	});
});

describe('Bicicleta.findById', () => {
	it('Debe devolver la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var a = new Bicicleta(1, 'verde', 'montaña', [-37.332211,-55.123456]);
		var b = new Bicicleta(2, 'naranja', 'urbana', [-30.654321, -50.132465]);
		Bicicleta.add(a);
		Bicicleta.add(b);

		var targetBici = Bicicleta.findById(1);

		expect(targetBici.id).toBe(1);
		expect(targetBici.color).toBe(a.color);
		expect(targetBici.modelo).toBe(a.modelo);
	});
});

describe('Bicicleta.removeById', () => {
	it('Debe eliminar la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var a = new Bicicleta(1, 'verde', 'montaña', [-37.332211,-55.123456]);
		var b = new Bicicleta(2, 'naranja', 'urbana', [-30.654321, -50.132465]);
		Bicicleta.add(a);
		Bicicleta.add(b);

		Bicicleta.removeById(1);

		expect(Bicicleta.allBicis.length).toBe(1);
	});
});

